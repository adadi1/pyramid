# Pyramid

To check if a given string is a pyramid word or not.

1. First install python and then Django framework.
2. To start the server type the below command
3. python manage.py runserver
4. once the server is up and running
5. Hit the link http://127.0.0.1:8000/check_string
6. Once you hit the link you will be asked to enter a string which checks if it is a pyramid word or not.
7. The ouput is in boolean value that is true or false.
8. I have placed a separate folder for screenshots where i have tested 2 use cases.