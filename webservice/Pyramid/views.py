from django.shortcuts import render, redirect
from django.http import HttpResponse
# Create your views here.
def check_string(request):
    context = {}
    if request.method =='POST':
        if request.POST.get('string_value') is not None:
            string = request.POST.get('string_value')
            result = check_for_pyramid_string(string)
            context["result"] = result
            return HttpResponse(result)
            #return render(request,'response.html', {'result': result})
        else:
            
            return render(request,'index.html')
    else:
        return render(request, 'index.html')

def check_for_pyramid_string(string):
    dict_str={}
    string=str(string)
    for i in string:
        if i in dict_str.keys():
            dict_str[i] += 1
        else:
            dict_str[i] = 1

    
    arr = list(dict_str.values())
    arr.sort()
    flag = 0
    for i in range(1, len(arr)):
        if abs(arr[i - 1] - arr[i]) != 1:
            return False
    return True
